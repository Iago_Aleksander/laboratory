// The purpose of the program is to allocate an array of ints
// and initialize them to a random number. But the program seg
// faults. Use gdb to find the error.
//
// gcc -Wall -g -o ex2 ex2.c

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(int argc, char* argv[]){

  printf("%d \n", INT_MAX);
  
  int n = SHRT_MAX;
  int A[n];
  int i = 0;
  while (i<10)
    {    
      A[i] = 12;
      printf("%d\n", A[i]);  
      i++; 
    }
  return EXIT_SUCCESS;
}
