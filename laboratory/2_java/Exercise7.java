

public class Exercise7 {

	// 7. Print Fibonacci series up to given number

	public static void main(String[] args) {
		
		int number = Console.askInt(" Until which number the Fibonacci sequence should be shown?: ");
		
		int fibbonacciNumber1 = 0;
		int fibbonacciNumber2 = 1;
		int temp = 0;
		
		while (fibbonacciNumber1 <= number)
		{
			System.out.print("  " + fibbonacciNumber1);
			
			temp = fibbonacciNumber1;
			fibbonacciNumber1 = fibbonacciNumber2;
			fibbonacciNumber2 = fibbonacciNumber1 + temp;
		}
	}

}
