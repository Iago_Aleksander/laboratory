
public class Exercise8 {

	// 8. Calculate factorial of an integer number
	
	public static int fatorial (int n)
	{
		if(n == 0)
			return 1;
		else 
			return n * fatorial(n-1);
	}
	public static void main(String[] args) {
		
		int number = Console.askInt(" Enter the number to be considered: ");
		long fatorial = 0;
		
		fatorial = fatorial(number);
		System.out.println(" The fatorial of " + number + " is " + fatorial +"!!!");
	}

}
