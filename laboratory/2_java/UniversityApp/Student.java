

import java.util.Calendar;
import java.util.Date;

public class Student {

	private String name;
	private String cpf;
	private int id;
	private Date birthday;
	private boolean active;
	private String course;
	private Date start_date;
	private Date finish_date;
	
	public Student() 
	{
		this ("?", "?", 0, 1900, 1, 1, false, "?");
	}
	
	public Student(String name, String cpf, int id, int year, int month, int day, boolean active, String course)
	{	
		this.name = name;
		this.cpf = cpf;
		this.id = id;
		this.active = active;	
		
		Calendar myCal; 
		myCal = Calendar.getInstance();
		myCal.set(Calendar.YEAR, year);
		myCal.set(Calendar.MONTH, month-1);
		myCal.set(Calendar.DAY_OF_MONTH, day);
		
		birthday = myCal.getTime();
		this.course = course;
		start_date = new Date();
		
		myCal = Calendar.getInstance();
		year = myCal.get(Calendar.YEAR);
		myCal.set(Calendar.YEAR, year);
		
		finish_date = myCal.getTime();
		
	}
	
	public void set_name (String name)
	{
		this.name = name;
	}
	
	public void set_cpf (String cpf)
	{
		this.cpf = cpf;
	}
	
	public void set_id (int id)
	{
		this.id = id;
	}
	
	public void set_birthday (int year, int month, int day)
	{
		Calendar myCal; 
		myCal = Calendar.getInstance();
		myCal.set(Calendar.YEAR, year);
		myCal.set(Calendar.MONTH, month-1);
		myCal.set(Calendar.DAY_OF_MONTH, day);
		
		birthday = myCal.getTime();
	}
	
	public void set_active (boolean active)
	{
		this.active = active;
	}
	
	public void set_course (String course)
	{
		this.course = course;
	}
	
	public void set_start_date (int year, int month, int day)
	{
		Calendar myCal; 
		myCal = Calendar.getInstance();
		myCal.set(Calendar.YEAR, year);
		myCal.set(Calendar.MONTH, month-1);
		myCal.set(Calendar.DAY_OF_MONTH, day);
		
		start_date = myCal.getTime();
	}
	
	public void set_finish_date (int year, int month, int day)
	{
		Calendar myCal; 
		myCal = Calendar.getInstance();
		myCal.set(Calendar.YEAR, year);
		myCal.set(Calendar.MONTH, month-1);
		myCal.set(Calendar.DAY_OF_MONTH, day);
		
		finish_date = myCal.getTime();
	}
	
	public String get_name()
	{
		return name;
	}
	
	public String get_cpf()
	{
		return cpf;
	}
	
	public int get_id()
	{
		return id;
	}
	
	public boolean get_active()
	{
		return active;
	}
	
	public Date get_birthday()
	{
		return birthday;
	}
	
	public String get_course()
	{
		return course;
	}
	
	public Date get_start_date()
	{
		return start_date;
	}
	
	public Date get_finish_date()
	{
		return finish_date;
	}
	
	public void print()
	{
		System.out.println("-----------------------------");
		System.out.println("Student Record:");
		System.out.println("Name: " +name);
		System.out.println("CPF: " +cpf);
		System.out.println("Id.: " +id);
		System.out.println("Birthday: " +birthday);
		if (active == true)
			System.out.println("Active");
		else
			System.out.println("Not Active");
		System.out.println("Course: " +course);
		System.out.println("Start Date: " +start_date);
		System.out.println("Finish Date: " +finish_date);
		System.out.println("-----------------------------\n");
	}
	
	public void ask()
	{
		String name = Console.askString("Insert name: ");
		set_name (name);
		
		String cpf = Console.askString("Insert CPF: ");
		set_cpf (cpf);
		
		int id = Console.askInt("Insert Id.: ");
		set_id (id);
		
		int day = Console.askInt("About the birthday. Insert the day: ");
		int month = Console.askInt("Insert the month: ");
		int year = Console.askInt("Insert the year: ");
		set_birthday(year, month, day);
		
		int active = Console.askInt("Insert 1 if the student is active or 0 if the student is not active: ");
		if (active == 1)
			set_active(true);	
		else
			set_active(false);
		
		String course = Console.askString("Insert course: ");
		set_course (course);
		
		day = Console.askInt("About the start date. Insert the day: ");
		month = Console.askInt("Insert the month: ");
		year = Console.askInt("Insert the year: ");
		set_start_date(year, month, day);
		set_finish_date(year+3,month,day);
	}
	
	public String toString()
	{
		return "\n-----------------------------\nStudent Record:\nName: " +name+ "\nCPF: " +cpf + "\nId.: " +id + "\nBirthday: " +birthday + "Course: " +course + "Start Date: " +start_date + "Finish Date: " +finish_date + "\n-----------------------------\n";
	}
}
