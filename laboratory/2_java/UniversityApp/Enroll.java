public class Enroll {

	public static void main(String[] args) {
		
		int number = Console.askInt("Insert the number of students to be enrolled: ");
		Student[] studentArray = new Student[number];

		for (int i = 0; i < number; i++)
		{
			studentArray[i] = new Student();
			studentArray[i].ask();
			System.out.println();
		}

		for (int i = 0; i < number; i++)
			studentArray[i].print();
		

	}

}

