


public class Exercise2 {

	/**
	 * 2. Find out if a number is prime
	 */
	public static void main(String[] args) {
		
		int number = Console.askInt(" Give a number to be checked: ");
		boolean prime = true;
		
		for (int i = 1; i <= number/2; i++)
			if (number % i == 0 && i !=1)
				prime = false;
		
		if (prime == true)
			System.out.println("The number is prime!!");
		else
			System.out.println(" The number is not prime!!");

	}

}
