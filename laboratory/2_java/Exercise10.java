
public class Exercise10 {
	
	// 10. Print all ASCII value table
	
	public static void main(String[] args) {
		
		System.out.println(" ASC II Table: \n");
		
		for (int i = 0; i < 128; i++)
			System.out.println((char) i);

	}

}
