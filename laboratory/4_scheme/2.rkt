;; 2. Find out if a number is prime

#lang racket

(define (is_prime n) 
  (is_prime2  n (- n 1)  ))

(define (is_prime2 n d) 
  (if (<= d 1) 
      'true 
      (if (= (modulo n d) 0) 
          'false 
          (is_prime2  n (- d 1)  ))))

(is_prime 7)
