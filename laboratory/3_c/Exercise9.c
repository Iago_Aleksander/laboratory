#include <stdio.h>

int main()
{
  int number1;
  int number2;
  int number3;

  printf (" Insert the first number to be considerated: ");
  scanf ("%d", &number1);

  printf (" Insert the second number to be considerated: ");
  scanf ("%d", &number2);

  printf (" Insert the third number to be considerated: ");
  scanf ("%d", &number3);

  int greatest = number1;
		
  if (number2 > greatest)
    greatest = number2;
		
  if (number3 > greatest)
    greatest = number3;

  printf (" The greatest number is %d !!!", greatest);
  printf ("\n");
}
