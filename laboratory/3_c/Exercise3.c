#include <stdio.h>

int main()
{
    int number;
    int temp;
    int length = 0;
    int palindrome = 1;
    int i = 1;
 

    printf(" Give a number to be checked: ");
    scanf("%d", &number);

    temp = number;

    if (temp == 0)
      length = 1;
    else
      while (temp != 0)
	{
	  temp = temp/10;
	  length++;
	}

    int numberVet[length];
    int j = length-1;

    for (i = 0; i < length; i++)
      {
	numberVet[i] = number % 10;
	number = number/10;
      }

    for (i = 0; i < (length/2); i++)
      {
	if (numberVet[i] != numberVet[j])
	palindrome = 0;
	j--;
      }

    if (palindrome == 1)
        printf("The number is palindrome!!\n");
    else
        printf(" The number is not palindrome!!\n");
}
