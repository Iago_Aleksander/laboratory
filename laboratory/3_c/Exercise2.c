#include <stdio.h>

int main()
{
    int number;
    int prime = 1;
    int i = 1;

    printf(" Give a number to be checked: ");
    scanf("%d", &number);

    for (i = 2; i <= (number/2); i++)
	if (number % i == 0)
	  prime = 0;

    if (prime == 1)
        printf("The number is prime!!\n");
    else
        printf(" The number is not prime!!\n");
}
